<?php
function getDB() {
    $dbhost="mydb.tamk.fi";
    $dbuser="c6mliffl"; // Your own username
    $dbpass="u9Fy3o5I"; // Your own password
    $dbname="dbc6mliffl62"; // Your own database name
    $dbConnection = new PDO("mysql:host=$dbhost;
	dbname=$dbname;charset=utf8", 
	$dbuser, $dbpass,array(PDO::MYSQL_ATTR_INIT_COMMAND 
	=> "SET NAMES 'utf8'"));
    
    return $dbConnection;
}

function executeQuery($sql, $array) {
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$result = $array != null ? $stmt->execute($array) : $stmt->execute();
		$object = $result ? $stmt->fetchAll(PDO::FETCH_OBJ) : null;
		$db = null;
		if(!$result)
			echo $stmt->debugDumpParams().'\n'.var_export($stmt->errorInfo());
		return $object;
	}catch(PDOException $p) {
		echo "executeQuery failed: " .$p->getMessage();
		return null;
	}
}

function executeClassQuery($sql, $array, $clazz) {
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $clazz);
		$result = $array != null ? $stmt->execute($array) : $stmt->execute();
		$object = $result ? $stmt->fetchAll() : null;
		$db = null;
		if(!$result)
			echo $stmt->debugDumpParams().'\n'.var_export($stmt->errorInfo());
		return $object;
	}catch(PDOException $p) {
		echo "executeQuery failed: " .$p->getMessage();
		return null;
	}
}

function executeInsert($sql, $array) {
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$result = $stmt->execute($array);
		$lastId = $db->lastInsertId();
		$db = null;
		if(!$result)
			echo $stmt->debugDumpParams().'\n'.var_export($stmt->errorInfo());
		return $lastId;
	}catch(PDOException $p) {
		echo "executeQuery failed: " .$p->getMessage();
		return null;
	}
}