<?php

/**
* 
*/
class BaseController
{
	protected $ci;
	
	function __construct($ci)
	{
		$this->ci = $ci;
	}

	public function getAll($req, $resp) {
		$json = $this->selectAll();
		if($json) {
			return $resp->withJson($json, 200);
		}
		return $resp->withStatus(204);
	}

	public function getOne($req,$resp) {
		$json = $this->selectOne($req->getAttribute('id'));
		if($json) {
			return $resp->withJson($json,200);
		}
		return $resp->withStatus(204);
	}

	public function create($req, $resp) {
		$json = $this->createNew(json_decode($req->getBody()));
		if($json) {
			return $resp->withJson($json, 201);
		}
		return $resp->withStatus(500);
	}

	public function update($req, $resp) {
		$json = $this->updateExisting($req->getAttribute('id'), json_decode($req->getBody()));
		if($json) {
			return $resp->withJson($json, 201);
		}
		return $resp->withStatus(500);	
	}

	public function delete($req, $resp) {
		$json = $this->deleteExisting($req->getAttribute('id'));
		if($json)
			return $resp->withStatus(200);
		return $resp->withStatus(500);
	}

}