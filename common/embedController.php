<?php

/**
* 
*/
class EmbeddedController
{
	protected $ci;
	
	function __construct($ci)
	{
		$this->ci = $ci;
	}

	public function getAll($req, $resp) {
		$json = $this->selectAll($req, $resp);
		if($json) {
			return $resp->withJson($json, 200);
		}
		return $resp->withStatus(204);
	}

	public function getOne($req,$resp) {
		$json = $this->selectOne($req, $resp);
		if($json) {
			return $resp->withJson($json,200);
		}
		return $resp->withStatus(204);
	}

	public function create($req, $resp) {
		$json = $this->createNew($req, $resp);
		if($json) {
			return $resp->withJson($json, 201);
		}
		return $resp->withStatus(500);
	}

	public function update($req, $resp) {
		$json = $this->updateExisting($req, $resp);
		if($json) {
			return $resp->withJson($json, 200);
		}
		return $resp->withStatus(500);	
	}

	public function delete($req, $resp) {
		$json = $this->deleteExisting($req, $resp);
		if($json) {
			return $json=='false' ? $resp->withStatus(204) : $resp->withJson($json, 200);
		}
		
		return $resp->withStatus(500);
	}

}