<?php

/**
*	@SWG\Definition(@SWG\xml(name="StationResponse"))
*/

class StationResponse implements JsonSerializable
{
	/**
	*	@SWG\Property(format="int64")
	*	@var int
	*/
	public $id;

	/**
	*	@SWG\Property(example="Station name")
	*	@var string
	*/
	public $name;

	/**
	*	@SWG\Property(example="Brand name")
	*	@var string
	*/
	public $brand;

	/**
	*	@SWG\Property(example="Company name")
	*	@var string
	*/
	public $company;

	/**
	*	@SWG\Property(example="type of the station")
	*	@var string
	*/
	public $type;

	/**
	*	@SWG\Property()
	*	@var string
	*/
	public $streetaddress;

	/**
	*	@SWG\Property(format="int64")
	*	@var int
	*/
	public $zipcode;

	/**
	*	@SWG\Property()
	*	@var string
	*/
	public $city;

	/**
	*	@SWG\Property(example="")
	*	@var string
	*/
	public $state;

	/**
	*	@SWG\Property(example="")
	*	@var string
	*/
	public $country;

	/**
	*	@SWG\Property(example=")
	*	@var string
	*/
	public $contact;

	/**
	*	@SWG\Property(format="double")
	*	@var double
	*/
	public $longitude;

	/**
	*	@SWG\Property(format="double")
	*	@var double
	*/
	public $latitude;

	public function JsonSerialize() {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'brand' => $this->brand
		];
	}
}

?>