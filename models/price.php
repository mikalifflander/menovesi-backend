<?php

/**
*	@SWG\Definition(
*		definition="PriceInsert",
*		@SWG\xml(
*			name="PriceInsert"
*		),
*		required={"fueltype_id", "pricePerUnit", "validFrom"}
*		@SWG\Property(
*			property="fueltype_id",
*			type="integer"
*		),
*		@SWG\Property(
*			property="pricePerUnit",
*			type="number",
*			format="double"
*		),
*		@SWG\Property(
*			property="validFrom",
*			description="Date time string. Describes time when price point is active. Format yyyy-mm-dd hh:MM:ss",
*			type="string"
*		)
*	)
*/

/**
*	@SWG\Definition(
*		definition="PriceInfo",
*		@SWG\xml(
*			name="PriceInfo"
*		),
*		@SWG\Property(
*			property="id",
*			type="integer"
*		),
*		@SWG\Property(
*			property="type",
*			type="string"
*		),
*		@SWG\Property(
*			property="pricePerUnit",
*			type="number",
*			format="double"
*		),
*		@SWG\Property(
*			property="validFrom",
*			description="Datetime value reperesenting time when price starts to be valid",
*			type="string"
*		),
*		@SWG\Property (
*			property="description",
*			description="Description for price information. Optional",
*			type="string"
*		)
*	)
*/
?>