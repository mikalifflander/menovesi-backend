<?php

/**
*	@SWG\Definition(@SWG\xml(name="NewStation"), required={"name", "brand_id", "company_id", "type_id", "manager_id"})
*/

class NewStation
{

	/**
	*	Station name
	*	@SWG\Property()
	*	@var string
	*/
	public $name;

	/**
	*	Brand id
	*	@SWG\Property(format="int64")
	*	@var int
	*/
	public $brand_id;

	/**
	*	Company id
	*	@SWG\Property(format="int64")
	*	@var int
	*/
	public $company_id;

	/**
	*	Id for station type
	*	@SWG\Property(format="integer")
	*	@var int
	*/
	public $type_id;

	/**
	*	Street address
	*	@SWG\Property()
	*	@var string
	*/
	public $streetaddress;

	/**
	*	Zipcode
	*	@SWG\Property(format="int64")
	*	@var int
	*/
	public $zipcode;

	/**
	*	City
	*	@SWG\Property()
	*	@var string
	*/
	public $city;

	/**
	*	State
	*	@SWG\Property(example="")
	*	@var string
	*/
	public $state;

	/**
	*	Country
	*	@SWG\Property(example="Finland")
	*	@var string
	*/
	public $country;

	/**
	*	Station managers user id
	*	@SWG\Property(format="int64")
	*	@var int
	*/
	public $manager_id;

	/**
	*	GPS Longitude
	*	@SWG\Property(type="number")
	*	@var double
	*/
	public $longitude;

	/**
	*	GPS Latitude
	*	@SWG\Property(format="Double")
	*	@var double
	*/
	public $latitude;

}

?>