<?php

/**
*	@SWG\Definition(
*		definition="UserInsert",
*		@SWG\xml(
*			name="UserInsert"
*		),
*		required={"firstname", "email"},
*		@SWG\Property(
*			property="firstname",
*			type="string"
*		),
*		@SWG\Property(
*			property="lastname",
*			type="string"
*		),
*		@SWG\Property(
*			property="email",
*			type="string"
*		),
*		@SWG\Property(
*			property="password",
*			type="string"
*		)
*	)
*/

/**
*	@SWG\Definition(
*		definition="UserInfo",
*		@SWG\xml(
*			name="UserInfo"
*		),
*		@SWG\Property ( 
*			property="id",
*			type="integer"
*		),
*		@SWG\Property(
*			property="firstname",
*			type="string"
*		),
*		@SWG\Property(
*			property="lastname",
*			type="string"
*		),
*		@SWG\Property(
*			property="email",
*			type="string"
*		)
*	)
*/
?>