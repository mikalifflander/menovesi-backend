<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET,PUT,POST,DELETE");
header("Access-Control-Allow-Headers: Content-Type");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'common/db.php';
require 'common/baseController.php';
require 'common/embedController.php';

/**
*	@SWG\Swagger(
*		schemes={"http"},
*		host="home.tamk.fi",
*		basePath="/~c6mliffl/cmd/practical/api/index.php",
*		@SWG\Info(
*			version="1.0",
*			title="menovesi.fi API documentation",
*			description="menovesi.fi (Thesis work) server API documentation. All rights reserved.",
*			@SWG\Contact(
*				email="mika.lifflander@eng.tamk.fi"
*			)
*		)
*	)
*/


/*
	Modules
*/
require 'controllers/stations.php';
require 'controllers/users.php';
require 'controllers/attributes.php';
require 'controllers/companies.php';
require 'controllers/vehicles.php';
require 'controllers/fuellings.php';
require 'controllers/prices.php';
require 'controllers/authentication.php';

$config = [
	'settings' => [
		'displayErrorDetails' => true,
	],
];
$app = new \Slim\App($config);

/* Stations */
$app->post('/stations', '\StationController:create');
$app->get('/stations', '\StationController:getAll');
$app->get('/stations/{id}', '\StationController:getOne');
$app->put('/stations/{id}', '\StationController:update');
$app->post('/stations/{id}/prices', '\StationController:addPriceForStation');
$app->get('/stations/{id}/prices', '\StationController:getPricesForStation');

/* Prices (utility) */
$app->get('/prices', '\PriceController:getAll');

/* Users */
$app->post('/users', '\UserController:create');
$app->get('/users', '\UserController:getAll');
$app->get('/users/{id}', '\UserController:getOne');
$app->put('/users/{id}', '\UserController:update');

/* Vehicles */
$app->get('/users/{uid}/vehicles', '\VehicleController:getAll');
$app->get('/users/{uid}/vehicles/{vid}', '\VehicleController:getOne');
$app->post('/users/{uid}/vehicles', '\VehicleController:create');
$app->put('/users/{uid}/vehicles/{vid}', '\VehicleController:update');
$app->delete('/users/{uid}/vehicles/{vid}', '\VehicleController:delete');

/* Fuellings */
$app->get('/users/{uid}/fuellings', '\FuellingController:getAll');
$app->post('/users/{uid}/vehicles/{vid}/fuellings', '\FuellingController:create');
$app->put('/users/{uid}/vehicles/{vid}/fuellings/{fid}', '\FuellingController:update');
$app->delete('/users/{uid}/vehicles/{vid}/fuellings/{fid}', '\FuellingController:delete');

/* Attributes */
$app->get('/attributes', '\AttributeController:selectAttributes');
$app->get('/stationtypes', '\AttributeController:selectStationTypes');
$app->get('/fueltypes', '\AttributeController:selectFuelTypes');
$app->get('/vehicletypes', '\AttributeController:selectVehicleTypes');

/* Companies */
$app->post('/companies', '\CompanyController:create');
$app->get('/companies', '\CompanyController:getAll');
$app->get('/companies/{id}', '\CompanyController:getOne');
$app->put('/companies/{id}', '\CompanyController:update');

/* Brands */
$app->get('/companies/{id}/brands', '\CompanyController:getBrandsForCompany');
$app->post('/companies/{id}/brands', '\CompanyController:addBrandForCompany');
$app->put('/companies/{cid}/brands/{bid}', '\CompanyController:updateBrandForCompany');

/* Authentication */
$app->post('/authentications', '\AuthenticationController:doLogin');

/*
REMAINDER:
$app->delete('/departments/{id}', '\StationController:delete');
*/

$app->run();