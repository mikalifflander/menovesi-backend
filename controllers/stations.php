<?php

/**
* 
*/

require 'models/stationResponse.php';

class StationController extends BaseController
{
    private $ALL_PRICES = "SELECT fp.id, fp.pricePerUnit as pricePerUnit, fp.description, fp.validFrom, ft.type
                from fuelprice fp, fueltype ft
                where ft.id = fp.fueltype_id and fp.station_id = ?
                order by fp.validFrom desc;";

    private $ACTIVE_PRICES = "";

    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    /**
    *   @SWG\GET(
    *       path="/stations",
    *       summary="Lists all stations",
    *       description="Lists all stations stored to the system with brand and company names populated",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/StationResponse")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored stations"
    *       )
    *   )
    */
    protected function selectAll() {
        $sql = "SELECT st.id, brand.name as brand, st.name, sttype.type as type, st.streetaddress, st.zipcode, st.city, st.state, st.country, st.longitude, st.latitude, user.email as contact
                from station as st, brand as brand, user as user, stationtype sttype
                where brand.id = st.brand_id and brand.company_id = st.brand_company_id and st.manager_user_id = user.id and sttype.id = st.stationtype_id;";
        return executeQuery($sql, null);
    }

    /**
    *   @SWG\GET(
    *       path="/stations/{id}",
    *       summary="Find one station by id",
    *       description="Finds a station with provided id. Brand and company names are populated",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/StationResponse")
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="Station with provided id was not found"
    *       )
    *   )
    */
    protected function selectOne($id) {
        $sql="SELECT st.id, brand.name as brand, st.name, sttype.type as type, st.streetaddress, st.zipcode, st.city, st.state, st.country, st.longitude, st.latitude, user.email as contact
                from station as st, brand as brand, user as user, stationtype sttype
                where brand.id = st.brand_id and brand.company_id = st.brand_company_id and st.manager_user_id = user.id and sttype.id = st.stationtype_id and st.id=?;";
        return executeQuery($sql, array($id));
    }

    /**
    *   @SWG\POST(
    *       path="/stations",
    *       summary="Creates a new station",
    *       description="Creates a new station with given values. Returns created station as a response",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Stations object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/NewStation")
    *       ),
    *       @SWG\Response(
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/StationResponse")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Station was not added to database due to error"
    *       )
    *   )
    */
    protected function createNew($data) {
        $sql="INSERT INTO station(station.name, station.brand_id, station.brand_company_id, station.stationtype_id, station.streetaddress, station.zipcode, station.city, station.state, station.country, station.longitude, station.latitude, station.manager_user_id) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        return $this->selectOne(executeInsert($sql, array($data->name, $data->brand_id, $data->company_id, $data->type_id, $data->streetaddress, $data->zipcode,$data->city,$data->state,$data->country,$data->longitude,$data->latitude,$data->manager_id)));
    }

    /**
    *   @SWG\PUT(
    *       path="/stations/{id}",
    *       summary="Update station",
    *       description="Updates station identified by id with provided data. Works as full update, fields not provided will be set as null or empty.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Updated object that needs to be saved to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/NewStation")
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/StationResponse")
    *       )
    *   )
    */
    protected function updateExisting($id, $data) {
         $sql = "UPDATE station SET station.name=?, station.brand_id=?, station.brand_company_id=?, station.stationtype_id=?, station.streetaddress=?, station.zipcode=?, station.city=?, station.state=?, station.country=?, station.longitude=?, station.latitude=?, station.manager_user_id=? where station.id=?";
         executeQuery($sql, array($data->name, $data->brand_id, $data->company_id, $data->stationtype, $data->streetaddress, $data->zipcode,$data->city,$data->state,$data->country,$data->longitude,$data->latitude,$data->manager_id,$id));
         return $this->selectOne($id);
    }

    protected function deleteExisting($id) {
        return null;
    }

    /**
    *   @SWG\GET(
    *       path="/stations/{id}/prices",
    *       summary="Gets all prices for station",
    *       description="Gets full price information for one station, descending order by valid from",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               ref="#/definitions/PriceInfo")
    *       )
    *   )
    */
    public function getPricesForStation($req, $resp) {
        $history = $req->getQueryParam('history', $default = false);
        $future = $req->getQueryParam('future', $default = false);

        $json = (object) [
            'current' => [],
            'future' => [],
            'history' => []
        ];

        $sql = "SELECT fp.id, fp.pricePerUnit as pricePerUnit, fp.description, fp.validFrom, ft.type
                from fuelprice fp, fueltype ft
                where ft.id = fp.fueltype_id and fp.station_id = ?
                order by fp.validFrom desc;";
        //$json = executeQuery($sql, array($req->getAttribute('id')));

        $json->current = executeQuery($sql, array($req->getAttribute('id')));

        if($json) {
            return $resp->withJson($json, 200);
        }
        return $resp->withStatus(204);
    }

    /**
    *   @SWG\POST(
    *       path="/stations/{id}/prices",
    *       summary="Adds new price point",
    *       description="Adds new price data for specified station. Fueltype id is required",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Stations object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/PriceInsert")
    *       ),
    *       @SWG\Response (
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               ref="#/definitions/PriceInfo"
    *           )
    *       ),
    *       @SWG\Response (
    *           response=405,
    *           description="Update failed"
    *       )
    *   )
    */
    public function addPriceForStation($req, $resp) {
        $data  = json_decode($req->getBody());
        $sql = "INSERT INTO fuelprice(station_id, fueltype_id, description, pricePerUnit, validFrom) values(?, ?, ?, ?, ?);";
        $id = executeInsert($sql, array($req->getAttribute('id'), $data->fueltype_id, $data->description, $data->pricePerUnit, $data->validFrom));

        $json = executeQuery("SELECT fp.id, fp.pricePerUnit as pricePerUnit, fp.description, fp.validFrom, ft.type
                from fuelprice fp, fueltype ft
                where ft.id = fp.fueltype_id and fp.id = ?;", array($id));
        if($json)
            return $resp->withJson($json, 201);

        return $resp->withStatus(405);
        
    }
}