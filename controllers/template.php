<?php

/**
* 
*/
class TemplateController extends BaseController
{
    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    protected function selectAll() {
        $sql = "SELECT department.id, department.dname, department.manager, employee.fname, employee.lname, department.mgrstartdate FROM department LEFT OUTER JOIN employee ON
            department.manager = employee.id";
        return executeQuery($sql, null);
    }

    protected function selectOne($id) {
        $sql="SELECT department.id, department.dname, department.manager, employee.fname, employee.lname, department.mgrstartdate FROM department LEFT OUTER JOIN employee ON department.manager = employee.id WHERE department.id=:id";
        return executeQuery($sql, array($id));
    }

    protected function createNew($data) {
        $sql="INSERT INTO department(department.dname, department.manager, department.mgrstartdate) values(?, ?, ?);";
        return $this->selectOne(executeInsert($sql, array($data->dname, $data->manager, $data->mgrstartdate)));
    }

    protected function updateExisting($id, $data) {
         $sql = "UPDATE department SET department.dname = ?, department.manager = ?, department.mgrstartdate = ? where department.id=?";
         executeQuery($sql, array($data->dname, $data->manager, $data->mgrstartdate,$id));
         return $this->selectOne($id);
    }

    protected function deleteExisting($id) {
          $sql = "DELETE from department where department.id=:id";
          executeQuery($sql, array($id));
          return true;
    }
}