<?php
/**
* @SWG\Definition(
*   definition="FuellingInfo",
*   @SWG\xml(
*     name="FuellingInsert"
*   ),
*   @SWG\Property(
*     property="id",
*     description="fuelling id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="license",
*     description="Vehicle license plate",
*     type="string"
*   ),
*   @SWG\Property(
*     property="units",
*     description="Total units fuelled",
*     type="number"
*   ),
*   @SWG\Property(
*     property="pricePerUnit",
*     description="Fuel price per unit",
*     type="number"
*   ),
*   @SWG\Property(
*     property="timestamp",
*     description="Fuelling timestamp",
*     type="string"
*   ),
*   @SWG\Property(
*     property="fulltank",
*     description="Was the tank filled up, 1=true, 0=false",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="brand",
*     description="Station brand where fuelling took place",
*     type="string"
*   ),
*   @SWG\Property(
*     property="station",
*     description="Name of the station where fuelling took place",
*     type="string"
*   ),
*   @SWG\Property(
*     property="odometer",
*     description="Odometer value at the time of the fuelling",
*     type="integer"
*   )
* )
*/

/**
* @SWG\Definition(
*   definition="FuellingInsert",
*   @SWG\xml(
*     name="FuellingInsert"
*   ),
*   required={"license", "fueltype_id", "vehicletype_id"},
*   @SWG\Property(
*     property="fuelprice_id",
*     description="Id for fuelprice info",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="units",
*     description="Total units fuelled",
*     type="number"
*   ),
*   @SWG\Property(
*     property="timestamp",
*     description="When the fuelling took place, format yyy-mm-dd HH:MM:ss",
*     type="string"
*   ),
*   @SWG\Property(
*     property="fulltank",
*     description="Was the tank filled up",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="odometer",
*     description="Odometer value at the time of fuelling",
*     type="integer"
*   )
* )
*/
class FuellingController extends EmbeddedController
{
    private $SALL_SQL = "SELECT f.id, v.license, f.units, fp.pricePerUnit, f.timestamp, f.fulltank, f.odometer, b.name as brand, st.name as station
                            FROM fuelling f, vehicle v, fuelprice fp, station st, brand b
                            WHERE v.id = f.vehicle_id 
                            and v.user_id = f.vehicle_user_id 
                            and fp.id=f.fuelprice_id 
                            and st.id=fp.station_id 
                            and b.id=st.brand_id 
                            and b.company_id=st.brand_company_id
                            and f.vehicle_user_id=?;";

    private $S1_SQL   = "SELECT f.id, v.license, f.units, fp.pricePerUnit, f.timestamp, f.fulltank, f.odometer, b.name as brand, st.name as station
                            FROM fuelling f, vehicle v, fuelprice fp, station st, brand b
                            WHERE v.id = f.vehicle_id 
                            and v.user_id = f.vehicle_user_id 
                            and fp.id=f.fuelprice_id 
                            and st.id=fp.station_id 
                            and b.id=st.brand_id 
                            and b.company_id=st.brand_company_id
                            and f.vehicle_user_id=?
                            and f.vehicle_id=?
                            and f.id=?;";

    private $UPD_SQL  = "UPDATE fuelling SET fuelprice_id=?, units=?, timestamp=?, fulltank=?, odometer=? WHERE vehicle_user_id=? and vehicle_id=? and id=?;";

    private $DEL_SQL  = "DELETE FROM fuelling WHERE vehicle_user_id=? and vehicle_id=? and id=?;";

    private $INS_SQL  = "INSERT INTO fuelling(vehicle_id, vehicle_user_id, fuelprice_id, units, timestamp, fulltank, odometer) VALUES(?,?,?,?,?,?,?);";

    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    /**
    *   @SWG\GET(
    *       path="/users/{uid}/fuellings",
    *       summary="List fuellings",
    *       description="Lists all fuellings for user",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/FuellingInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="User has no fuellings"
    *       )
    *   )
    */
    protected function selectAll($req, $resp) {
        return executeQuery($this->SALL_SQL, array($req->getAttribute('uid')));

    }

    protected function selectOne($req, $resp) {
         return $resp->withStatus(501);
    }

    /**
    *   @SWG\POST(
    *       path="/users/{uid}/vehicles/{vid}/fuellings",
    *       summary="Adds new fuelling information for specified vehicle",
    *       description="Creates new fuelling entry to database, returns created object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Fuelling object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/FuellingInsert")
    *       ),
    *       @SWG\Response(
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/FuellingInfo")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Operation failed"
    *       )
    *   )
    */
    protected function createNew($req, $resp) {
      
        $data = json_decode($req->getBody());
        $uid = $req->getAttribute('uid');
        $vid = $req->getAttribute('vid');
        $fid = executeInsert($this->INS_SQL, array($uid, $vid, $data->fuelprice_id, $data->units, $data->timestamp, $data->fulltank, $data->odometer));
        return executeQuery($this->S1_SQL, array($uid, $vid, $fid));
    }

    /**
    *   @SWG\PUT(
    *       path="/users/{uid}/vehicles/{vid}/fuellings/{fid}",
    *       summary="Updates a fuelling info",
    *       description="Updates fuelling info, returns updated object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Fuelling object that needs to be updated",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/FuellingInsert")
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *             type="array",
    *             @SWG\Items(ref="#/definitions/FuellingInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Update failed"
    *       )
    *   )
    */
    protected function updateExisting($req, $resp) {
        $uid = $req->getAttribute('uid');
        $vid = $req->getAttribute('vid');
        $fid = $req->getAttribute('fid');

        $data = json_decode($req->getBody());
       
        executeQuery($this->UPD_SQL, array($data->fuelprice_id, $data->units, $data->timestamp, $data->fulltank, $data->odometer,$uid, $vid, $fid));
        return executeQuery($this->S1_SQL, array($uid, $vid, $fid));
    }


    /**
    *   @SWG\DELETE(
    *       path="/users/{uid}/vehicles/{vid}/fuellings/{fid}",
    *       summary="Deletes a fuelling info",
    *       description="Deletes fuelling info, returns list of fuellings as a response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *             type="array",
    *             @SWG\Items(ref="#/definitions/FuellingInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Deletion failed."
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored fuellings."
    *       )
    *   )
    */
    protected function deleteExisting($req, $resp) {
      $uid = $req->getAttribute('uid');
      $vid = $req->getAttribute('vid');
      $fid = $req->getAttribute('fid');

      executeQuery($this->DEL_SQL, array($uid, $vid, $fid));
      $json = executeQuery($this->SALL_SQL, array($uid));
      return $json ? $json : 'false';
    }
}