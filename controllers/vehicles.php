<?php
/**
* @SWG\Definition(
*   definition="VehicleInfo",
*   @SWG\xml(
*     name="VehicleInfo"
*   ),
*   @SWG\Property(
*     property="id",
*     description="Vehicle id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="license",
*     description="Vehicle license plate",
*     type="string"
*   ),
*   @SWG\Property(
*     property="make",
*     description="Vehicle make",
*     type="string"
*   ),
*   @SWG\Property(
*     property="model",
*     description="Vehicle model",
*     type="string"
*   ),
*   @SWG\Property(
*     property="engine",
*     description="Vehicle engine size",
*     type="number"
*   ),
*   @SWG\Property(
*     property="year",
*     description="Vehicle year model",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="vehicleType",
*     description="Vehicle type",
*     type="string"
*   ),
*   @SWG\Property(
*     property="fuelType",
*     description="Fuel type",
*     type="string"
*   )
* )
*/

/**
* @SWG\Definition(
*   definition="VehicleInsert",
*   @SWG\xml(
*     name="VehicleInsert"
*   ),
*   required={"license", "fueltype_id", "vehicletype_id"},
*   @SWG\Property(
*     property="license",
*     description="Vehicle license plate",
*     type="string"
*   ),
*   @SWG\Property(
*     property="make",
*     description="Vehicle make",
*     type="string"
*   ),
*   @SWG\Property(
*     property="model",
*     description="Vehicle model",
*     type="string"
*   ),
*   @SWG\Property(
*     property="engine",
*     description="Vehicle engine size",
*     type="number"
*   ),
*   @SWG\Property(
*     property="year",
*     description="Vehicle year model",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="vehicletype_id",
*     description="Vehicle type id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="fueltype_id",
*     description="Fuel type id",
*     type="integer"
*   )
* )
*/
class VehicleController extends EmbeddedController
{
    private $SALL_SQL = "SELECT v.id, v.license, v.make, v.model, v.engine, v.year, ft.type as fuelType, vt.type as vehicleType
                          FROM vehicle v, fueltype ft, vehicletype vt
                          WHERE v.fueltype_id = ft.id and vt.id = v.vehicletype_id and v.user_id=?;";

    private $S1_SQL   = "SELECT v.id, v.license, v.make, v.model, v.engine, v.year, ft.type as fuelType, vt.type as vehicleType
                          FROM vehicle v, fueltype ft, vehicletype vt
                          WHERE v.fueltype_id = ft.id and vt.id = v.vehicletype_id and v.user_id=? and v.id=?;";

    private $UPD_SQL  = "UPDATE vehicle SET license=?, make=?, model=?, engine=?, year=?, fueltype_id=?, vehicletype_id=?
                          WHERE vehicle.user_id=? and vehicle.id=?;";

    private $DEL_SQL  = "DELETE FROM vehicle WHERE vehicle.user_id=? and vehicle.id=?;";

    private $INS_SQL  = "INSERT INTO vehicle(license, make, model, engine, year, fueltype_id, vehicletype_id, user_id)
                          VALUES(?, ?, ?, ?, ?, ?, ?, ?);";

    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    /**
    *   @SWG\GET(
    *       path="/users/{uid}/vehicles",
    *       summary="List vehicles",
    *       description="Lists all vehicles for user",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/VehicleInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored vehicles"
    *       )
    *   )
    */
    protected function selectAll($req, $resp) {
        return executeQuery($this->SALL_SQL, array($req->getAttribute('uid')));

    }

    /**
    *   @SWG\GET(
    *       path="/users/{uid}/vehicles/{vid}",
    *       summary="Get one vehicle",
    *       description="Gets one vehicle for user. User is identified by uid and vehicle by vid",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/VehicleInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="Vehicle was not found"
    *       )
    *   )
    */
    protected function selectOne($req, $resp) {
         return executeQuery($this->S1_SQL, array($req->getAttribute('uid'), $req->getAttribute('vid')));
    }

    /**
    *   @SWG\POST(
    *       path="/users/{uid}/vehicles",
    *       summary="Creates a new vehicle",
    *       description="Creates new vehicle to database, returns created object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Vehicle object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/VehicleInsert")
    *       ),
    *       @SWG\Response(
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/VehicleInfo")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Adding vehicle to database failed"
    *       )
    *   )
    */
    protected function createNew($req, $resp) {
      
        $data = json_decode($req->getBody());
        $uid = $req->getAttribute('uid');
        $vid = executeInsert($this->INS_SQL, array($data->license, $data->make, $data->model,$data->engine, $data->year,$data->fueltype_id,$data->vehicletype_id, $uid));
        return executeQuery($this->S1_SQL, array($uid, $vid));
    }

    /**
    *   @SWG\PUT(
    *       path="/users/{uid}/vehicles/{vid}",
    *       summary="Updates a vehicle",
    *       description="Updates vehicle, returns updated object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Vehicle object that needs to be updated",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/VehicleInsert")
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *             type="array",
    *             @SWG\Items(ref="#/definitions/VehicleInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Updating vehicle to database failed"
    *       )
    *   )
    */
    protected function updateExisting($req, $resp) {
        $uid = $req->getAttribute('uid');
        $vid = $req->getAttribute('vid');
        $data = json_decode($req->getBody());
       
        executeQuery($this->UPD_SQL, array($data->license, $data->make, $data->model,$data->engine,$data->year,$data->fueltype_id, $data->vehicletype_id, $uid, $vid));
        return executeQuery($this->S1_SQL, array($uid, $vid));
    }


    /**
    *   @SWG\DELETE(
    *       path="/users/{uid}/vehicles/{vid}",
    *       summary="Deletes a vehicle",
    *       description="Deletes vehicle, returns list of vehicles as a response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *             type="array",
    *             @SWG\Items(ref="#/definitions/VehicleInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Deletion failed."
    *       )
    *   )
    */
    protected function deleteExisting($req, $resp) {
      $uid = $req->getAttribute('uid');
      $vid = $req->getAttribute('vid');

      executeQuery($this->DEL_SQL, array($uid, $vid));
      
      return executeQuery($this->SALL_SQL, array($uid));
    }
}