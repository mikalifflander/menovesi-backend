<?php
/**
 * Created by PhpStorm.
 * User: mika
 * Date: 11/01/2017
 * Time: 18.35
 */

class AuthenticationController extends BaseController {

    private $LOGIN_SQL = "SELECT u.id, u.firstname, u.lastname, u.email from user u WHERE u.email=? and u.password=?;";

    protected $ci;
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    public function doLogin($req, $resp) {
        $credentials = json_decode($req->getBody());

        $json = executeQuery($this->LOGIN_SQL,array($credentials->email, $credentials->password));

        if($json) return $resp->withJson($json, 200);

        return $resp->withStatus(403);
    }
}