<?php

/**
* 
*/
class UserController extends BaseController
{
    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    /**
    *   @SWG\GET(
    *       path="/users",
    *       summary="Gets all users",
    *       description="List all users",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *             type="array",
    *             ref="#/definitions/UserInfo")
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No users found"
    *       )
    *   )
    */
    protected function selectAll() {
        $sql = "SELECT usr.id, usr.firstname, usr.lastname, usr.email from user usr;";
        return executeQuery($sql, null);
    }

    /**
    *   @SWG\GET(
    *       path="/users/{id}",
    *       summary="Get one user",
    *       description="Get one user, identified by id",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *             ref="#/definitions/UserInfo")
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="user was not found"
    *       )
    *   )
    */
    protected function selectOne($id) {
          $sql = "SELECT usr.id, usr.firstname, usr.lastname, usr.email from user usr WHERE usr.id = ?;";
        return executeQuery($sql, array($id));
    }


    /**
    *   @SWG\POST(
    *       path="/users",
    *       summary="Creates a new user",
    *       description="Creates new user to database, returns created user object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="User object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/UserInsert")
    *       ),
    *       @SWG\Response(
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/UserInfo")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Adding user to database failed"
    *       )
    *   )
    */
    protected function createNew($data) {
        $sql="INSERT INTO user(firstname, lastname, email, salt, user.password) VALUES(?, ?, ?, ?, ?);";
        return $this->selectOne(executeInsert($sql, array($data->firstname, $data->lastname, $data->email, $this->randomString(4),$data->password)));
    }

    /**
    *   @SWG\PUT(
    *       path="/users/{id}",
    *       summary="Update user",
    *       description="Updates user to database, returns user object as response. User is identified by id",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="User object that needs to be updated to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/UserInsert")
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/UserInfo")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Update failed"
    *       )
    *   )
    */
    protected function updateExisting($id, $data) {
       $sql = "UPDATE user
               SET firstname=?, lastname=?, email=?, password=?, salt=?
               WHERE user.id = ?;";
             
        executeQuery($sql, array($data->firstname, $data->lastname, $data->email,$data->password, $this->randomString(4), $id));
       return $this->selectOne($id);
    }

    protected function deleteExisting($id) {
       return null;
    }

    private function randomString($length) { 
      $pull = [];
      while (count($pull) < $length) {
        $pull = array_merge($pull, range(0, 9), range('a', 'z'), range('A', 'Z'));
      }
      shuffle($pull);
      return substr(implode($pull), 0, $length);
    }
}