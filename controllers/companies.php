<?php
/**
* @SWG\Definition(
*   definition="BrandInsert",
*   @SWG\xml(
*     name="BrandInsert"
*   ),
*   required={"name"},
*   @SWG\Property(
*     property="name",
*     description="Brand name",
*     type="string"
*   ),
*   @SWG\Property(
*     property="description",
*     description="Brand description",
*     type="string"
*   )
* )
*/

/**
* @SWG\Definition(
*   definition="BrandInfo",
*   @SWG\xml(
*     name="BrandInfo"
*   ),
*   @SWG\Property (
*     property="id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="name",
*     description="Brand name",
*     type="string"
*   ),
*   @SWG\Property(
*     property="company",
*     description="Company name",
*     type="string"
*   ),
*   @SWG\Property(
*     property="description",
*     description="Brand description",
*     type="string"
*   )
* )
*/


/**
* @SWG\Definition(
*   definition="CompanyInfo",
*   @SWG\xml(
*     name="CompanyInfo"
*   )
*   @SWG\Property (
*     property="id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="name",
*     description="Company name",
*     type="string"
*   )
* )
*/

/**
* @SWG\Definition(
*   definition="CompanyInsert",
*   @SWG\xml(
*     name="CompanyInsert"
*   ),
*   required={"name"},
*   @SWG\Property(
*     property="name",
*     description="Company name",
*     type="string"
*   )
* )
*/

class CompanyController extends BaseController
{
    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    /**
    *   @SWG\GET(
    *       path="/companies",
    *       summary="Lists all companies",
    *       description="Lists all companies in database",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/CompanyInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored companies"
    *       )
    *   )
    */
    protected function selectAll() {
        $sql = "SELECT c.id, c.name FROM company c;";
        return executeQuery($sql, null);
    }
    /**
    *   @SWG\GET(
    *       path="/companies/{id}",
    *       summary="Get one company",
    *       description="Get one company, identified by id",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/CompanyInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="Company was not found"
    *       )
    *   )
    */
    protected function selectOne($id) {
        $sql="SELECT c.id, c.name FROM company c WHERE c.id=?;";
        return executeQuery($sql, array($id));
    }

    /**
    *   @SWG\POST(
    *       path="/companies",
    *       summary="Creates a new company",
    *       description="Creates new company to database, returns created object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Company object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/CompanyInsert")
    *       ),
    *       @SWG\Response(
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/CompanyInfo")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Adding company to database failed"
    *       )
    *   )
    */
    protected function createNew($data) {
        $sql="INSERT INTO company(name) values(?);";
        return $this->selectOne(executeInsert($sql, array($data->name)));
    }
 
    /**
    *   @SWG\PUT(
    *       path="/companies/{id}",
    *       summary="Update company",
    *       description="Updates company to database, returns updated object as response. Company is identified by id",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Company object that needs to be updated to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/CompanyInsert")
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/CompanyInfo")
    *       ),
    *       @SWG\Response(
    *           response=500,
    *           description="Update failed"
    *       )
    *   )
    */
    protected function updateExisting($id, $data) {
         $sql = "UPDATE company SET company.name = ? where company.id=?";
         executeQuery($sql, array($data->name, $id));
         return $this->selectOne($id);
    }

    protected function deleteExisting($id) {
        return false;
    }

    /**
    *   @SWG\GET(
    *       path="/companies/{id}/brands",
    *       summary="Lists all brands for company",
    *       description="Lists all brands associated with given company.",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/BrandInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="Company does not have any brands"
    *       )
    *   )
    */
    public function getBrandsForCompany($req, $resp) {
        $sql = "SELECT b.id, c.name as company, b.name, b.description
                from brand b, company c
                where b.company_id = c.id and b.company_id=?;";

        $json = executeQuery($sql, array($req->getAttribute('id')));

        return $json ? $resp->withJson($json, 200) : $resp->withStatus(204);
    }

    /**
    *   @SWG\POST(
    *       path="/companies/{id}/brands",
    *       summary="Creates a new brannd",
    *       description="Creates new brand to database and associates it with company. returns created object as response.",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Brand object that needs to be added to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/BrandInsert")
    *       ),
    *       @SWG\Response(
    *           response=201,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/BrandInfo")
    *       ),
    *       @SWG\Response(
    *           response=400,
    *           description="Adding company to database failed"
    *       )
    *   )
    */
    public function addBrandForCompany($req, $resp) {
      $data = json_decode($req->getBody());
      $cid = $req->getAttribute('id');

      $sql = "INSERT INTO brand(name, description, company_id) VALUES(?,?,?);";
      $id = executeInsert($sql, array($data->name, $data->description, $cid));

      $sql = "SELECT b.id, c.name as company, b.name, b.description
                from brand b, company c
                where b.company_id = c.id and b.company_id=? and b.id=?";

      $json = executeQuery($sql, array($cid, $id));
      return $json ? $resp->withJson($json, 201) : $resp->withStatus(400);
    }
    
    /**
    *   @SWG\PUT(
    *       path="/companies/{cid}/brands/{bid}",
    *       summary="Update brand",
    *       description="Updates brand to database, returns updated object as response. Company is identified by cid, brand by bid",
    *       consumes={"application/json"},
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="body",
    *           in="body",
    *           description="Brand object that needs to be updated to database",
    *           required=true,
    *           @SWG\Schema(ref="#/definitions/BrandInsert")
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(ref="#/definitions/BrandInfo")
    *       ),
    *       @SWG\Response(
    *           response=400,
    *           description="Update failed"
    *       )
    *   )
    */
    public function updateBrandForCompany($req, $resp) {
      $data = json_decode($req->getBody());

      $cid = $req->getAttribute('cid');
      $bid = $req->getAttribute('bid');

      $sql = "UPDATE brand SET name=?, description=? WHERE brand.id=? and brand.company_id=?;";
      executeQuery($sql, array($data->name, $data->description, $bid, $cid));

        $sql = "SELECT b.id, c.name as company, b.name, b.description
                from brand b, company c
                where b.company_id = c.id and b.company_id=? and b.id=?";

      $json = executeQuery($sql, array($cid, $bid));
      return $json ? $resp->withJson($json, 200) : $resp->withStatus(400);
    }
}