<?php

/**
* @SWG\Definition(
*   definition="StationType",
*   @SWG\xml(
*     name="StationType"
*   ),
*   @SWG\Property (
*     property="id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="type",
*     description="Station type",
*     type="string"
*   )
* )
*/

/**
* @SWG\Definition(
*   definition="VehicleType",
*   @SWG\xml(
*     name="VehicleType"
*   ),
*   @SWG\Property (
*     property="id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="type",
*     description="Vehicle type",
*     type="string"
*   )
* )
*/

/**
* @SWG\Definition(
*   definition="FuelType",
*   @SWG\xml(
*     name="FuelType"
*   ),
*   @SWG\Property (
*     property="id",
*     type="integer"
*   ),
*   @SWG\Property(
*     property="type",
*     description="Fuel type",
*     type="string"
*   )
* )
*/
class AttributeController extends BaseController
{
    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    protected function selectAll() {
      return null;
    }

    protected function selectOne($id) {
        return null;
    }

    protected function createNew($data) {
       return null;
    }

    protected function updateExisting($id, $data) {
        return null;
    }

    protected function deleteExisting($id) {
         return false;
    }

    /**
    *   @SWG\GET(
    *       path="/vehicletypes",
    *       summary="Lists all vehicle types",
    *       description="Lists all vehicle types in database",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/VehicleType")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored vehicle types"
    *       )
    *   )
    */
    public function selectVehicleTypes($req, $resp) {
      $sql = "SELECT vt.id, vt.type FROM vehicletype vt;";
      $json = executeQuery($sql, null);

      return $json ? $resp->withJson($json, 200) : $resp->withStatus(204);
    }

    /**
    *   @SWG\GET(
    *       path="/fueltypes",
    *       summary="Lists all fuel types",
    *       description="Lists all fuel types in database",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/FuelType")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored fuel types"
    *       )
    *   )
    */
    public function selectFuelTypes($req, $resp) {
      $sql = "SELECT ft.id, ft.type FROM fueltype ft;";
      $json = executeQuery($sql, null);

      return $json ? $resp->withJson($json, 200) : $resp->withStatus(204);
    }
  
   /**
    *   @SWG\GET(
    *       path="/stationtypes",
    *       summary="Lists all station types",
    *       description="Lists all station types in database",
    *       produces={"application/json"},
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/StationType")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored station types"
    *       )
    *   )
    */
    public function selectStationTypes($req, $resp) {
      $sql = "SELECT st.id, st.type FROM stationtype st;";
      $json = executeQuery($sql, null);

      return $json ? $resp->withJson($json, 200) : $resp->withStatus(204);
    }

    public function selectAttributes($req, $resp)
    {
        $sql = "SELECT vt.id, vt.type FROM vehicletype vt;";
        $vts = executeQuery($sql, null);

        $sql = "SELECT ft.id, ft.type FROM fueltype ft;";
        $fts = executeQuery($sql, null);

        $sql = "SELECT st.id, st.type FROM stationtype st;";
        $sts = executeQuery($sql, null);

        $sql = "SELECT c.id, c.name FROM company c;";
        $comps = executeQuery($sql, null);

        $json = (object) [
            'fueltypes' => $fts,
            'stationtypes' => $sts,
            'vehicletypes' => $vts,
            'companies' => $comps
        ];

        return $resp->withJson($json, 200);
    }
}