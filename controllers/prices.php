<?php

/**
*   @SWG\Definition(
*       definition="StationPriceInfo",
*       @SWG\xml(
*           name="StationPriceInfo"
*       ),
*       @SWG\Property(
*           property="brand",
*           description="Brand name",
*           type="string"
*       ),
*       @SWG\Property(
*           property="station",
*           description="Station name",
*           type="string"
*       ),
*       @SWG\Property (
*           property="type",
*           description="Fuel type",
*           type="string"
*       ),
*       @SWG\Property (
*           property="description",
*           description="Description for price information. Optional",
*           type="string"
*       ),
*       @SWG\Property(
*           property="pricePerUnit",
*           type="number",
*           format="double",
*           description="Fuel price per single unit, i.e Litre"
*       ),
*       @SWG\Property(
*           property="validFrom",
*           description="ISO8601 date reperesenting time when price starts to be valid",
*           type="string"  
*       )
*   )
*/

class PriceController extends EmbeddedController
{

    private $ALL_SQL = "SELECT b.name as brand, st.name as station, ft.type, fp.description, fp.pricePerUnit, fp.validFrom, unix_timestamp(fp.validFrom) as validFromTs, st.longitude as lng, st.latitude as lat
                        from station st, brand b, 
                            fueltype ft, 
                            fuelprice fp INNER JOIN (
                                select station_id, fueltype_id, max(validFrom) as mxdate
                                from fuelprice
                                where validFrom < now()
                                group by station_id, fueltype_id) mfp
                            ON fp.station_id = mfp.station_id and fp.validFrom = mfp.mxdate and fp.fueltype_id = mfp.fueltype_id
                        where ft.id = fp.fueltype_id and st.id = fp.station_id and b.id = st.brand_id and b.company_id = st.brand_company_id;";

    private $ONE_SQL = "SELECT b.name as brand, st.name as station, ft.type, fp.description, fp.pricePerUnit, fp.validFrom, unix_timestamp(fp.validFrom) as validFromTs, st.longitude as lng, st.latitude as lat
                            from station st, brand b, 
                            fueltype ft, 
                            fuelprice fp INNER JOIN (
                                SELECT station_id, max(validFrom) as mxdate
                                from fuelprice
                                where fueltype_id = ?
                                and validFrom < now()
                                group by station_id) mfp
                            ON fp.station_id = mfp.station_id and fp.validFrom = mfp.mxdate
                            where ft.id = fp.fueltype_id and st.id = fp.station_id 
                            and b.id = st.brand_id and b.company_id = st.brand_company_id;";

    protected $ci;   
    function __construct($ci)
    {
        $this->ci = $ci;
    }

    /**
    *   @SWG\GET(
    *       path="/prices",
    *       summary="List prices",
    *       description="Lists prices for all stations.",
    *       produces={"application/json"},
    *       @SWG\Parameter(
    *           name="fueltype",
    *           description="Id for requested fueltype to limit the results to cover only single fueltype",
    *           required=false,
    *           type="integer",
    *           in="query"
    *       ),
    *       @SWG\Response(
    *           response=200,
    *           description="Succesfull operation",
    *           @SWG\Schema(
    *               type="array",
    *               @SWG\Items(ref="#/definitions/StationPriceInfo")
    *           )
    *       ),
    *       @SWG\Response(
    *           response=204,
    *           description="No stored priceinfo"
    *       )
    *   )
    */
    protected function selectAll($req, $resp) {
        $fueltype = $req->getQueryParam("fueltype", $default = null);

        return $fueltype ? executeQuery($this->ONE_SQL, array($fueltype)) : executeQuery($this->ALL_SQL, null);
     }

    /* Unimplemented methods */    
    protected function selectOne($req, $resp) {
         return null;
    }
    
    protected function createNew($req, $resp) { 
       return null;
    }
  
    protected function updateExisting($req, $resp) {
        return null;
    }
    protected function deleteExisting($req, $resp) {
      return null;
    }
}